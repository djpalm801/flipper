package main

import (
	"fmt"
	"github.com/djpalmer801/flipper/pkg/restaurant"
	"go.uber.org/zap"
	"gopkg.in/alecthomas/kingpin.v2"
	"os"
	"path/filepath"
)

func main() {
	svcName := "flipper"
	cfg := struct {
		pancakeOrderCount int
		pancakeStack      string
	}{}

	a := kingpin.New(filepath.Base(os.Args[0]), svcName)

	// Command line arguments
	a.Flag("pancake-order-count", "number of pancakes to order").Short('p').Default("5").IntVar(&cfg.pancakeOrderCount)
	a.Flag("pancake-order-stack", "supply a stack of pancakes using + smiley up and - for smiley down. example: --++-+-").Short('s').StringVar(&cfg.pancakeStack)

	l, _ := zap.NewProduction()

	// Parse command line arguments
	_, err := a.Parse(os.Args[1:])
	if err != nil {
		l.Fatal("failed to parse command line arguments", zap.Error(err))
		a.Usage(os.Args[1:])
		os.Exit(2)
	}

	var w restaurant.Waiter
	if cfg.pancakeStack != "" {
		// retrieve specifically oriented pancake stack
		w, err = restaurant.RetrieveSpecificPancakeStack(cfg.pancakeStack)
		if err != nil {
			l.Fatal("", zap.Error(err))
			os.Exit(2)
		}
	} else {
		// retrieve random stack of pancakes
		w, err = restaurant.OrderPancakes(cfg.pancakeOrderCount)
		if err != nil {
			l.Fatal("", zap.Error(err))
			os.Exit(2)
		}
	}

	// Wrap waiter with logging middleware
	w = restaurant.NewLoggingMiddleware(*l)(w)
	// Use XRay vision to log the pancakes received with a bad orientation
	l.Info(fmt.Sprintf("pancakes received from kitchen: %s", w.XRay()))
	// Flip the pancakes prior to serving them
	w.Flip()
	// Use XRay vision to log the pancakes after they've been properly sorted
	l.Info(fmt.Sprintf("pancakes ready for delivery: %s", w.XRay()))
}
