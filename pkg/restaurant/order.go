package restaurant

import (
	"fmt"
	"math/rand"
	"regexp"
	"strings"
	"time"
)

//	OrderPancakes takes an order count between 1 - 100 pancakes
//	and returns a pancake struct which holds a stack of randomly
//	oriented pancakes.
func OrderPancakes(pancakeCount int) (Waiter, error) {
	// if the order is invalid return an error
	if pancakeCount > 100 || pancakeCount < 1 {
		return waiter{}, fmt.Errorf("you can order anywhere from 1 to 100 pancakes. %d pancake(s) is not a valid order", pancakeCount)
	}

	var stack []bool
	// the chef will place the pancakes in a stack
	// with a random orientation
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < pancakeCount; i++ {
		stack = append(stack, rand.Intn(2) == 1)
	}

	return waiter{
		stack: stack,
	}, nil
}

//	RetrieveSpecificPancakeStack takes a string of pancakes (e.g. "+-+---+")
//	and returns a waiter with the stack of pancakes.
func RetrieveSpecificPancakeStack(pancakeStack string) (Waiter, error) {
	//	check pancakeStack pattern to make sure it only includes + or -
	//	characters
	ok, err := regexp.MatchString("^[+-]*$", pancakeStack)
	if err != nil || !ok {
		return nil, fmt.Errorf("pancake stack provided is invalid. must only contain + and - chars")
	}

	var stack []bool
	s := strings.Split(pancakeStack, "")
	for _, v := range s {
		if v == "+" {
			stack = append(stack, true)
		} else {
			stack = append(stack, false)
		}
	}
	return waiter{
		stack: stack,
	}, nil
}
