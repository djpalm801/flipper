package restaurant

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestOrderPancakes_Success(t *testing.T) {
	p, err := OrderPancakes(5)
	assert.Nil(t, err)
	assert.NotNil(t, p)
}

func TestOrderPancakes_InvalidCount(t *testing.T) {
	_, err := OrderPancakes(0)
	assert.NotNil(t, err)
	_, err = OrderPancakes(101)
	assert.NotNil(t, err)
}

func TestRetrieveSpecificPancakeStack(t *testing.T) {
	w, err := RetrieveSpecificPancakeStack("+++---+-")
	assert.Nil(t, err)
	assert.Equal(t, top+top+top+bottom+bottom+bottom+top+bottom, w.XRay())
}
