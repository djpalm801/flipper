package restaurant

//	validate that waiter implements Waiter interface
var _ Waiter = waiter{}

const (
	top    = "\u263A" // smiley face in unicode
	bottom = "\u25EF" // empty circle in unicode
)

//	Waiter is an interface that contains
//	Flip() and String() methods
type Waiter interface {
	Flip() int
	XRay() string
}

//	stack represents the stack of pancakes.
//	each index in the slice of booleans represents a pancake.
//	true = smiley facing up, false = smiley facing down
//	waiter implements the Waiter interface
type waiter struct {
	stack []bool
}

//	Flip sorts a stack of pancakes
//	and returns a count of how many flips it
//	took to sort the stack with smiley faces up
func (w waiter) Flip() int {
	flipCount := 0
	for i := 1; i < len(w.stack); i++ {
		// check if the current pancake has the
		// same orientation as the last pancake
		// if they match, no need to flip
		if w.stack[i] == w.stack[i-1] {
			continue
		}
		// put the spatula under the desired pancake
		topStack := w.stack[0:i]
		// flip the top stack of pancakes
		for i, cake := range topStack {
			topStack[i] = !cake
		}
		bottomStack := w.stack[i:]
		// place the top stack back down on top of the bottom stack
		topStack = append(topStack, bottomStack...)

		flipCount++
	}

	// if the stack is upside down, flip one last time
	if w.stack[0] == false {
		flipCount++
		for i, n := range w.stack {
			w.stack[i] = !n
		}
	}
	return flipCount
}

// XRay iterates over the stack of pancakes and
// returns a string representation of the stack
func (w waiter) XRay() string {
	result := ""
	for _, cake := range w.stack {
		if cake {
			result = result + top
			continue
		}
		result = result + bottom
	}
	return result
}
