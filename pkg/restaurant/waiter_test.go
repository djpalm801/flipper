package restaurant

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestWaiter_Flip(t *testing.T) {
	tests := map[string]struct {
		expectedFlips int
		stack         []bool
	}{
		"-": {
			expectedFlips: 1,
			stack:         []bool{false},
		},
		"-+": {
			expectedFlips: 1,
			stack:         []bool{false, true},
		},
		"+-": {
			expectedFlips: 2,
			stack:         []bool{true, false},
		},
		"+++": {
			expectedFlips: 0,
			stack:         []bool{true, true, true},
		},
		"--+-": {
			expectedFlips: 3,
			stack:         []bool{false, false, true, false},
		},
		// add additional test cases here
	}

	for test, tt := range tests {
		t.Run(test, func(t *testing.T) {
			w := waiter{stack: tt.stack}
			assert.Equal(t, tt.expectedFlips, w.Flip())
		})
	}
}

func TestWaiter_String(t *testing.T) {
	w := waiter{[]bool{false, false, true, false}}
	assert.Equal(t, bottom+bottom+top+bottom, w.XRay())
}
