package restaurant

import (
	"fmt"
	"go.uber.org/zap"
	"time"
)

const (
	Method   = "method"
	Duration = "duration"
)

var _ Waiter = loggingMiddleware{}

type LoggingMiddleware func(waiter Waiter) Waiter

type loggingMiddleware struct {
	Logger zap.Logger
	Next   Waiter
}

//	NewLoggingMiddleware initializes a logging wrapper for a Waiter
func NewLoggingMiddleware(l zap.Logger) LoggingMiddleware {
	return func(next Waiter) Waiter {
		return loggingMiddleware{
			Logger: l,
			Next:   next,
		}
	}
}

//  Flip is a middleware function that will log the flipCount each
//  time the Flip() method is invoked.
func (m loggingMiddleware) Flip() (flipCount int) {
	defer func(begin time.Time) {
		m.Logger.Info(fmt.Sprintf("it took the waiter %d flip(s) to prepare the pancakes",
			flipCount), zap.String("caller", "Flip"), zap.String(Method, "flip"), zap.Duration(Duration, time.Since(begin)))
	}(time.Now())
	return m.Next.Flip()
}

//  XRay is a middleware function that currently isn't implemented.
//	If logging is needed on XRay we can implement this method.  Otherwise,
//	its purpose is to satisfy the implementation of the Waiter interface.
func (m loggingMiddleware) XRay() string {
	return m.Next.XRay()
}
