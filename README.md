# flipper

### Run Instructions
 go run cmd/flipper/main.go --help to show available command line args
* --pancake-order-count to generate a stack of pancakes that are oriented randomly in the stack.
    * go run cmd/flipper/main.go -p=10
* --pancake-order-stack="--++--+" to specify the orientation of your pancake order as received by the waiter
    * go run cmd/flipper/main.go -s="--++--+"
